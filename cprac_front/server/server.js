const express = require("express")
const bodyParser = require("body-parser")
const axios = require("axios")
const cheerio = require("cheerio")
const app = express()
const cors = require("cors")
const port = process.env.PORT || 8081
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get("/api/crawling", cors(), (req, res) => {
  let ulList = []
  const getHtml = async () => {
    try {
      return await axios.get("https://www.marketwatch.com/latest-news")
    } catch (error) {
      console.error(error)
    }
  }
  getHtml()
    .then(html => {
      const $ = cheerio.load(html.data)
      const bodyList = $("div.group").children("div.element")

      bodyList.each(function(i, item) {
        const resImg = $(this)
          .find("a.figure__image img")
          .attr("data-srcset")

        const encodeImg = resImg != null ? resImg.split(" ") : ""
        ulList[i] = {
          title: $(this)
            .find("h3.article__headline a")
            .text(),
          date: $(this)
            .find("div.article__details span.article__timestamp")
            .text(),
          url: $(this)
            .find("h3.article__headline a")
            .attr("href"),
          img: encodeImg[0] != null ? encodeImg[0] : ""
        }
      })
      return ulList
    })
    .then(axiosRes => {
      // let transList = []
      // for (let i = 0; i < 8; i++) {
      //   transList[i] = axiosRes[i]
      //   axios
      //     .post("https://openapi.naver.com/v1/papago/n2mt", null, {
      //       data: { source: "en", target: "ko", text: axiosRes[i].title },
      //       headers: {
      //         "X-Naver-Client-Id": "ajmbj5u8qMOvIhsDMGWb",
      //         "X-Naver-Client-Secret": "jFA7A2vBjT"
      //       }
      //     })
      //     .then(transRes => {
      //       transList[i].title = transRes.data.message.result.translatedText
      //       if (i === 7) {
      //         res.send(transList)
      //       }
      //     })
      //     .catch(function(err) {
      //       console.log(err)
      //     })
      // }
      res.send(axiosRes)
    })
})

app.listen(port, () => console.log(`Listening on port ${port}`))
