import React from "react"
import { Route, BrowserRouter, Switch } from "react-router-dom"

//page
import MainPage from "../pages/MainPage"
import NewsPage from "../pages/NewsPage"

//css
import "../style/contents.css"

function App() {
  return (
    <div className="wrap">
      <BrowserRouter>
        <Switch>
          <Route path="/usanews" component={NewsPage} />
          <Route exact path="/" component={MainPage} />
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default App
