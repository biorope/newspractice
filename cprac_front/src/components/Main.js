import React, { useState, useEffect } from "react"
import CommonContainer from "../containers/CommonContainer"
import ContentContainer from "../containers/ContentContainer"
import { useSelector, useDispatch } from "react-redux"
import { usaAxiosCallRequest } from "../store/actions"

const Main = props => {
  const dispatch = useDispatch()
  const usaNewsData = useSelector(state => state.usaaxios)

  useEffect(() => {
    dispatch(usaAxiosCallRequest())
  }, [])

  const [hideFlag, setHideFlag] = useState(true)
  return (
    <div className="container">
      <CommonContainer hideFlag={hideFlag} setHideFlag={setHideFlag} />
      <ContentContainer
        title="Translate Economy News"
        subtitle="각 국 경제 관련 뉴스 번역"
        hideFlag={hideFlag}
        setHideFlag={setHideFlag}
      />
    </div>
  )
}
export default Main
