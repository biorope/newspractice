import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import CommonContainer from "../../containers/CommonContainer";
import ContentContainer from "../../containers/ContentContainer";
import { useSelector, useDispatch } from "react-redux";
import Usa from "./Usa";
import { usaAxiosCallRequest } from "../../store/actions";
import PopUpContent from "./PopUpContent";

const UsaNews = () => {
  const [hideFlag, setHideFlag] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  const dispatch = useDispatch();
  const newsData = useSelector((state) => state.NewsReducer);

  useEffect(() => {
    dispatch(usaAxiosCallRequest());
  }, []);

  const showData = (item, date, url, img, idx) => {
    const splitDate = date.split(" ");
    const changemonth = (month) => {
      switch (month) {
        case "Jan.":
          return "1";
        case "Feb.":
          return "2";
        case "Mar.":
          return "3";
        case "Apr.":
          return "4";
        case "May.":
          return "5";
        case "Jun.":
          return "6";
        case "Jul.":
          return "7";
        case "Aug.":
          return "8";
        case "Sep.":
          return "9";
        case "Oct.":
          return "10";
        case "Nov.":
          return "11";
        case "Dec.":
          return "12";
        default:
          return "none";
      }
    };

    const encodeDay = splitDate[0] !== "" ? splitDate[1].split(",") : "";
    const encodeDate =
      splitDate[0] !== ""
        ? splitDate[2] +
          "-" +
          changemonth(splitDate[0]) +
          "-" +
          encodeDay[0] +
          " " +
          (splitDate[5] === "a.m." ? "오전 " : "오후 ") +
          splitDate[4] +
          " USA"
        : "시간 없음";

    return (
      idx < 8 && (
        <div className="col-lg-3 col-md-6 mb-4 mb-lg-3" key={idx}>
          <div className="card rounded shadow-sm border-0">
            <div className="card-body p-4">
              <img
                src={
                  img !== "" ? img : require("../../assets/img/none_img.jpg")
                }
                alt=""
                className="card-img d-block mx-auto mb-3"
                onClick={() => {
                  setShowPopup(true);
                }}
              />
              <h5>
                <Link
                  to="#"
                  onClick={() => {
                    window.open(url, "_blank");
                  }}
                  className="text-dark"
                >
                  {item}
                </Link>
              </h5>
              <p className="small text-muted font-italic">{encodeDate}</p>
            </div>
          </div>
        </div>
      )
    );
  };
  return (
    <div className="container">
      <CommonContainer hideFlag={hideFlag} />
      <ContentContainer
        title="Usa Economy News"
        subtitle="미국 경제 뉴스 번역"
        hideFlag={hideFlag}
        setHideFlag={setHideFlag}
        contentArea={<Usa newsData={newsData} showData={showData} />}
      />
      {showPopup === true && (
        <PopUpContent showPopup={showPopup} setShowPopup={setShowPopup} />
      )}
    </div>
  );
};

export default UsaNews;
