import React from "react";

const PopUpContent = ({ targetData, showPopup, setShowPopup }) => {
  return (
    <div>
      <div className="box">
        <div>
          <div>
            <div>
              <div>
                <div className="img_box">
                  <img src={require("../../assets/img/none_img.jpg")}></img>
                  <div className="scrap">
                    <button className="btn btn-dark">스크랩</button>
                  </div>
                </div>
                <div className="custom-control custom-radio mr-2">
                  <input type="radio"></input>
                  <label>파파고</label>
                </div>
              </div>
              <div>
                <h1>title</h1>
                <h2 className="content">
                  contentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontent
                </h2>
              </div>
            </div>
            <img
              className="close_btn"
              src={require("../../assets/img/close.png")}
              onClick={() => {
                setShowPopup(false);
              }}
            />
          </div>
        </div>
      </div>
      <div
        className="mask"
        onClick={() => {
          setShowPopup(false);
        }}
      ></div>
    </div>
  );
};

export default PopUpContent;
