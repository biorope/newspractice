import React from "react";

const Usa = ({ newsData, showData }) => {
  return (
    <div className="row pb-5 mb-4">
      {newsData.data !== null &&
        newsData.data.usaNewsList.map((items, idx) => {
          return (
            items.title !== "" &&
            showData(items.title, items.date, items.url, items.img, idx)
          );
        })}
    </div>
  );
};

export default Usa;
