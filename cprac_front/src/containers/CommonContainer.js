import React, { useEffect } from "react"
import { Link } from "react-router-dom"

const CommonContainer = ({ hideFlag }) => {
  return (
    <div>
      <div
        className="vertical-nav bg-light"
        style={
          hideFlag !== true ? { marginLeft: "-17em" } : { marginLeft: "0" }
        }
        id="sidebar"
      >
        <div className="py-4 px-3 mb-4 bg-light">
          <div className="media d-flex align-items-center">
            <img
              src={require("../assets/img/profile_img.jpg")}
              alt="..."
              width="65"
              className="mr-3 rounded-circle img-thumbnail shadow-sm"
            />
            <div className="media-body">
              <h4 className="m-0">Daniel Lee</h4>
              <p className="font-weight-light text-muted mb-0">ver. beta</p>
            </div>
          </div>
        </div>

        <p className="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">
          Main
        </p>

        <ul className="nav flex-column bg-white mb-0">
          <li className="nav-item">
            <Link to="/" className="nav-link text-dark font-italic bg-light">
              <i className="fa fa-th-large mr-3 text-primary fa-fw"></i>
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/usanews"
              className="nav-link text-dark font-italic bg-light"
            >
              <i className="fa fa-address-card mr-3 text-primary fa-fw"></i>
              UsaNews
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/chinanews"
              className="nav-link text-dark font-italic bg-light"
            >
              <i className="fa fa-address-card mr-3 text-primary fa-fw"></i>
              ChinaNews
            </Link>
          </li>
        </ul>

        <p className="text-gray font-weight-bold text-uppercase px-3 small py-4 mb-0">
          Charts
        </p>

        <ul className="nav flex-column bg-white mb-0">
          <li className="nav-item">
            <Link to="#" className="nav-link text-dark font-italic">
              <i className="fa fa-area-chart mr-3 text-primary fa-fw"></i>
              Area charts
            </Link>
          </li>
          <li className="nav-item">
            <Link to="#" className="nav-link text-dark font-italic">
              <i className="fa fa-bar-chart mr-3 text-primary fa-fw"></i>
              Bar charts
            </Link>
          </li>
          <li className="nav-item">
            <Link to="#" className="nav-link text-dark font-italic">
              <i className="fa fa-pie-chart mr-3 text-primary fa-fw"></i>
              Pie charts
            </Link>
          </li>
          <li className="nav-item">
            <Link to="#" className="nav-link text-dark font-italic">
              <i className="fa fa-line-chart mr-3 text-primary fa-fw"></i>
              Line charts
            </Link>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default CommonContainer
