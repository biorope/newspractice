import React, { useState, useEffect } from "react";

const ContentContainer = ({
  title,
  subtitle,
  contentArea,
  hideFlag,
  setHideFlag,
}) => {
  const fnTransBtn = (e) => {
    e.target.id === "jb-radio-2" && e.target.checked && console.log("hi");
    e.target.id === "jb-radio-1" && e.target.checked && console.log("hi");
  };
  return (
    <div
      className={"page-content p-5"}
      style={hideFlag === true ? {} : { marginLeft: "-17rem" }}
      id="content"
    >
      <button
        type="button"
        className="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"
        onClick={() => {
          hideFlag === true ? setHideFlag(false) : setHideFlag(true);
        }}
      >
        <i className="fa fa-bars mr-2"></i>
        <small className="text-uppercase font-weight-bold">
          {hideFlag === true ? "메뉴 바 숨기기" : "메뉴 바 보이기"}
        </small>
      </button>
      <h2 className="display-4 text-white">{title}</h2>
      <p className="lead text-white mb-0">{subtitle}</p>
      <div className="row pl-3 pr-3 float-right text-white">
        <div className="custom-control custom-radio mr-2">
          <input
            type="radio"
            name="transNews"
            className="custom-control-input"
            id="jb-radio-1"
            onClick={(e) => {
              fnTransBtn(e);
            }}
          ></input>
          <label className="custom-control-label" for="jb-radio-1">
            파파고 번역
          </label>
        </div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            name="transNews"
            className="custom-control-input"
            id="jb-radio-2"
            onClick={(e) => {
              fnTransBtn(e);
            }}
          ></input>
          <label className="custom-control-label" for="jb-radio-2">
            구글 번역
          </label>
        </div>
      </div>
      <div className="separator"></div>

      {contentArea}
    </div>
  );
};
export default ContentContainer;
