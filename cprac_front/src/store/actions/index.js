export const USA_AXIOS_CALL_REQUEST = "USA_AXIOS_CALL_REQUEST";
export const USA_AXIOS_CALL_SUCCESS = "USA_AXIOS_CALL_SUCCESS";
export const USA_AXIOS_CALL_FAIL = "USA_AXIOS_CALL_FAIL";

export function usaAxiosCallRequest(payload) {
  return {
    type: USA_AXIOS_CALL_REQUEST,
    payload,
  };
}

export function usaAxiosCallSuccess(data) {
  return {
    type: USA_AXIOS_CALL_SUCCESS,
    data,
  };
}

export function usaAxiosCallFail(error) {
  return {
    type: USA_AXIOS_CALL_FAIL,
    error,
  };
}
