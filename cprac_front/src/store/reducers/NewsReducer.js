import * as actions from "../actions";

const initialState = {
  data: null,
};

export const reducer = (state = initialState, action, payload) => {
  switch (action.type) {
    case actions.USA_AXIOS_CALL_REQUEST:
      return {
        ...state,
        payload,
      };
    case actions.USA_AXIOS_CALL_SUCCESS:
      return {
        ...state,
        data: action.data,
      };
    case actions.USA_AXIOS_CALL_FAIL:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};
