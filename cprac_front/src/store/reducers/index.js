import { combineReducers } from "redux"
import { reducer as NewsReducer } from "./NewsReducer"

const rootReducer = combineReducers({
  NewsReducer
})

export default rootReducer
