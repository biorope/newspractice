import { call, put, takeEvery, all, fork } from "redux-saga/effects";
import * as actions from "../actions";
import axios from "axios";

function* fetchUsaAxiosCallSaga(action) {
  try {
    if (!action.payload) {
      const data = yield call([axios, "get"], "/api/crawling");
      const usaNewsList = [];
      for (let i = 0; i < 20; i++) {
        usaNewsList.push(data.data[i]);
      }
      yield put({
        type: actions.USA_AXIOS_CALL_SUCCESS,
        data: { usaNewsList },
      });
    } else {
      const data = action.payload;
      console.log(data);
      // yield put({ type: actions.USA_AXIOS_CALL_SUCCESS, data: { data } });
    }
  } catch (error) {
    yield put(actions.usaAxiosCallFail(error.response));
  }
}

function* watchUsaAxiosCall() {
  yield takeEvery(actions.USA_AXIOS_CALL_REQUEST, fetchUsaAxiosCallSaga);
}

export default function* usaRequestSaga() {
  yield all([fork(watchUsaAxiosCall)]);
}
