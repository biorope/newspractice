import { all } from "redux-saga/effects"
import usaRequestSaga from "./NewsSaga"

export default function* rootSaga() {
  yield all([usaRequestSaga()])
}
